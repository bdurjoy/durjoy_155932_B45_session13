<?php


class Person
{

    public $name;
    public $address;
    public $phone;
    public static $myStaticProperty;

    public function __call($name, $arguments)
    {
        echo "I',m inside " . __METHOD__ . "<br>";
        echo "wrong Method = $name";

        echo "<pre>";
        print_r($arguments);
        echo "</pre>";
    }

    public static function __callStatic($name, $arguments)
    {
        echo "I',m inside ".__METHOD__."<br>";
        echo "wrong Static Method = $name";

        echo "<pre>";
        print_r($arguments);
        echo "</pre>";
    }

    public function doSomething(){
        echo "I'm doing something<br>";
    }
}


Person::$myStaticProperty = "Hello World";
echo Person::$myStaticProperty;